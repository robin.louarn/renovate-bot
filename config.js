/**
 * @type {import('renovate/dist/config/types').RenovateConfig}
 */
module.exports = {
  platform: 'gitlab',
  endpoint: process.env.CI_API_V4_URL,
  repositoryCache: 'enabled',
  logFile: `${process.env.CI_PROJECT_DIR}/renovate-log.ndjson`,
  onboardingConfig: {
    $schema: 'https://docs.renovatebot.com/renovate-schema.json',
    extends: ['config:recommended'],
  },
  baseDir: `${process.env.CI_PROJECT_DIR}/renovate`,
}
