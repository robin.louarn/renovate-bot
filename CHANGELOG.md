# sofa-renovate

## 1.7.2

### Patch Changes

- 5fbe85c: montées des versions

## 1.7.1

### Patch Changes

- a100e1b: montées des versions

## 1.7.0

### Minor Changes

- 99e0dfc: fix onboarding

## 1.6.0

### Minor Changes

- 99e0dfc: fix onboarding

## 1.5.0

### Minor Changes

- Configuration de artifactory

## 1.4.0

### Minor Changes

- Mise en place de la convention gitmoji

## 1.3.0

### Minor Changes

- 78370ac: 👷 Mise en place du cache gitlab

## 1.2.0

### Minor Changes

- a91df00: Modification de la stratégie d'instalation

## 1.1.0

### Minor Changes

- ffaf484: Ajout d'un groupName pour les dépendances sofa

## 1.0.0

### Major Changes

- 6c2c194: ✨ Mise en plase de la configuration de renovate-bot
