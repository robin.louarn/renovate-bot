# sofa-renovate

## Description

Ce projet permet le lancement de Renovate par GitLab. En phase de développement, il permet aussi l'exécution de Renovate en local afin de ne pas être dépendant de la CI.

## Installation

- Installer le projet

```sh
pnpm install
```

- Configurer les variables d'environnement (ce projet utilise dotenv pour gérer les variables d'environnement en local)

```sh
cp example.env .env
```

> Il faudra alors ajouter vos tokens : `GITHUB_COM_TOKEN` pour télécharger les changelogs, `RENOVATE_TOKEN` pour l'accès à GitLab, `RENOVATE_REPOSITORIES` avec la liste des projets que vous voulez scanner, ou bien avec le flag `--autodiscover` en utilisant `RENOVATE_EXTRA_FLAGS` par exemple.

## Usage

- Validation de la configuration

```sh
pnpm test
```

- Lancement de Renovate

```sh
pnpm dev
```

> Pour lancer Renovate en mode debug, il faut éditer la variable `LOG_LEVEL=debug` dans le fichier `.env`

## Renovate

- `config.js` contient la configuration dynamique de Renovate
- `renovate.json` contient la configuration statique de Renovate
- `default.json` contient le preset par défaut du projet, exemple d'utilisation :

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": ["local>robin.louarn/renovate-bot"]
}
```
